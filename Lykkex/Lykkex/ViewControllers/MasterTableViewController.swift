//
//  MasterTableViewController.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/18/18.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit
import RxDataSources
import RxSwift

class MasterTableViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    
    let bag = DisposeBag()
    var dataSource: RxTableViewSectionedReloadDataSource<SectionModel<String, (AssetPair, Asset?)>>?
    
    var assets = BehaviorSubject<[Asset]?>(value: [])
    var assetPairs = BehaviorSubject<[AssetPair]?>(value: [])
    
    // MARK:- Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "masterCell")
        let dataSource = RxTableViewSectionedReloadDataSource<SectionModel<String, (AssetPair, Asset?)>>(configureCell: { (section, tableview, indexPath, item) -> UITableViewCell in
            
            let cell = tableview.dequeueReusableCell(withIdentifier: "masterCell", for: indexPath)
            
            //TODO: Config cell
            cell.textLabel?.text = item.0.name
            cell.detailTextLabel?.text = item.1?.name
            
            
            return cell
        })
        
        self.dataSource = dataSource
        
        // Combine all
       
        let observable = Observable.combineLatest(assets.asObservable(), assetPairs.asObservable()){ responseAssets,responsePair -> [(AssetPair, Asset?)] in
            
            let touples : [(AssetPair, Asset?)] = responsePair!.map{ pairValue in
            
                let filterArray = responseAssets!.filter({ (assetvalue) -> Bool in
                    
                    return pairValue.baseAssetID == assetvalue.id
                })
                
                return (pairValue, filterArray.first)
            }
            
            return touples
        }
        
        
        observable
            .map{ toupeItem in
                [SectionModel(model: "", items: toupeItem)]
            }
            .observeOn(MainScheduler.instance)
            .bind(to: self.tableView.rx.items(dataSource: dataSource))
            .disposed(by: bag)

        loadData()
    }
    
    //MARK:- Load data
    private func loadData(){
        
        NetworkRequestService.assets().observeOn(MainScheduler.asyncInstance).subscribe(assets.asObserver()).disposed(by: bag)
        NetworkRequestService.assetPairs().observeOn(MainScheduler.asyncInstance).subscribe(assetPairs.asObserver()).disposed(by: bag)
    }
    
    //MARK:- Button actions
    
    @IBAction func logoutAction(_ sender: UIBarButtonItem) {
        
        if let delegate = UIApplication.shared.delegate as? AppDelegate{
            sender.isEnabled = false
            UserDefaults.standard.set(nil, forKey: AppUserDefaults.lastLoginUser.rawValue)
            delegate.setHomeScreen()
        }
    }
    
    /*
    func requestPrices(){
     
     NetworkRequestService.assetsDetails(assetPairId: (items.first?.baseAssetID)!).subscribe(onNext:{ element in
     print(element)
     }).disposed(by: bag)
     
        Observable<Int>.timer(0, period: 10, scheduler: MainScheduler.instance)
            .startWith(-1)
            .map{ _ in (
                print("Request")
                )
            }
            .flatMapLatest{ _ in
                // TODO: Get data
            }
            .subscribe(onNext:{ [weak self] element in
                
                if element != nil{
                    
                }
            })
            .disposed(by: bag)
    }
 */
}
