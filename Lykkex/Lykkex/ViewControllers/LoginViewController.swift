//
//  LoginViewController.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/17/18.
//  Copyright © 2018 Home. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftKeychainWrapper

class LoginViewController: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    let bag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let emailValid = emailTextField.rx.text
            .map{ value in
                ValidationUtil.isValidEmail(value)
            }
            .share(replay: 1)
        
        emailValid
            .bind(to: loginButton.rx.isEnabled)
            .disposed(by: bag)
    }

    @IBAction func loginAction(_ sender: UIButton) {
        
        sender.isEnabled = false
        self.activityIndicator.startAnimating()
        
        NetworkRequestService.auth(email: emailTextField.text!, password: "3e70b949fa539ab1244fdc67168796541c0e0d8db9e928924952fe4de0751112")
            .observeOn(MainScheduler.instance)
            .do{ [weak self] in
                sender.isEnabled = true
                self?.activityIndicator.stopAnimating()
            }
            .subscribe(onNext: { [weak self] (data) in
                
                // Store to keychain
                guard let `self` = self else{
                    return
                }
                
                // Store to keychain
                guard KeychainHellper.storeUser(user: data, email: self.emailTextField.text!) else{
                    
                    let alert = UIAlertController(title: "Alert", message: NSLocalizedString("Not able to store user in Keychain", comment: ""), preferredStyle: .alert)
                    let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                    alert.addAction(action)
                    self.present(alert, animated: true, completion: nil)
                    return
                }
                
                // Set last login user
                UserDefaults.standard.set(self.emailTextField.text!, forKey: AppUserDefaults.lastLoginUser.rawValue)
                
                // Go to home screens
                let storyboard = UIStoryboard.init(name: "Home", bundle: nil)
                if let vc = storyboard.instantiateInitialViewController(){
                    self.present(vc, animated: true, completion: nil)
                }
                
            }, onError: { (error) in
                
                let alert = UIAlertController(title: "Alert", message: error.localizedDescription, preferredStyle: .alert)
                let action = UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil)
                alert.addAction(action)
                self.present(alert, animated: true, completion: nil)
            })
            .disposed(by: bag)
    }
    
}
