//
//  NetowrkRequestService.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/17/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import RxSwift
import Moya

enum ApiError: Error {
    case badRequest
    case notAuthorized
    case serverError
    case badData(error : String)
}

extension ApiError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .badRequest:
            return NSLocalizedString("Bad request", comment: "My error")
            
        case .notAuthorized:
            return NSLocalizedString("Not authorized for this action", comment: "")
            
        case .serverError:
            return NSLocalizedString("Server error", comment: "")
            
        case .badData(error: let errorDesc):
            return errorDesc
        }
    }
}

class NetworkRequestService {
    
    static func auth(email: String, password: String) -> Observable<User>{
    
        let provider = MoyaProvider<MyRequestService>()
        
        return Observable.create { (subscriber) -> Disposable in
            
            _ = provider.rx.request(.auth(userName: email, password: password))
                .subscribe(onSuccess:
                    { (response) in
                        if 200 ..< 300 ~= response.statusCode {
                            
                            do{
                            let user = try response.map(User.self)
                                subscriber.onNext(user)
                            }
                            catch{
                                let errorMessage = try? response.map(ErrorMessage.self)
                                subscriber.onError(ApiError.badData(error: errorMessage?.message ?? "Unknown error"))
                            }
                            
                        } else if 400 ..< 500 ~= response.statusCode {
                            subscriber.onError(ApiError.badRequest)
                        } else {
                            subscriber.onError(ApiError.serverError)
                        }
                        subscriber.onCompleted()
                }, onError: { (error) in
                    
                    print(error.localizedDescription)
                    subscriber.onError(error)
                })
            
            return Disposables.create()
        }
    }
    
    
    static func assetPairs() -> Observable<[AssetPair]?>{
        
        let provider = MoyaProvider<MyRequestService>()
        
        return Observable.create { (subscriber) -> Disposable in
            
            _ = provider.rx.request(.assetPairs)
                .subscribe(onSuccess:
                    { (response) in
                        if 200 ..< 300 ~= response.statusCode {
                            
                            do{
                                let responseAssetPairs = try response.map(AssetPairsResponse.self, atKeyPath: "Result", using: JSONDecoder(), failsOnEmptyData: true)
                                subscriber.onNext(responseAssetPairs.assetPairs)
                            }
                            catch{
                                let errorMessage = try? response.map(ErrorMessage.self)
                                subscriber.onError(ApiError.badData(error: errorMessage?.message ?? "Unknown error"))
                            }
                            
                        } else if 400 ..< 500 ~= response.statusCode {
                            subscriber.onError(ApiError.badRequest)
                        } else {
                            subscriber.onError(ApiError.serverError)
                        }
                        subscriber.onCompleted()
                }, onError: { (error) in
                    
                    print(error.localizedDescription)
                    subscriber.onError(error)
                })
            
            return Disposables.create()
        }
    }
    
    static func assets() -> Observable<[Asset]?>{
        
        let provider = MoyaProvider<MyRequestService>()
        
        return Observable.create { (subscriber) -> Disposable in
            
            _ = provider.rx.request(.asset)
                .subscribe(onSuccess:
                    { (response) in
                        if 200 ..< 300 ~= response.statusCode {
                            
                            do{
                                let responseAssets = try response.map(AssetsResponse.self, atKeyPath: "Result", using: JSONDecoder(), failsOnEmptyData: true)
                                subscriber.onNext(responseAssets.assets)
                            }
                            catch{
                                let errorMessage = try? response.map(ErrorMessage.self)
                                subscriber.onError(ApiError.badData(error: errorMessage?.message ?? "Unknown error"))
                            }
                            
                        } else if 400 ..< 500 ~= response.statusCode {
                            subscriber.onError(ApiError.badRequest)
                        } else {
                            subscriber.onError(ApiError.serverError)
                        }
                        subscriber.onCompleted()
                }, onError: { (error) in
                    
                    print(error.localizedDescription)
                    subscriber.onError(error)
                })
            
            return Disposables.create()
        }
    }
    
    static func assetsDetails(assetPairId: String) -> Observable<AssetPairDetails>{
        
        let provider = MoyaProvider<MyRequestService>()
        
        return Observable.create { (subscriber) -> Disposable in
            
            _ = provider.rx.request(.lastPrice(assetPairId: assetPairId))
                .subscribe(onSuccess:
                    { (response) in
                        if 200 ..< 300 ~= response.statusCode {
                            
                            do{
                                let assetDetails = try response.map(AssetPairDetails.self)
                                subscriber.onNext(assetDetails)
                            }
                            catch{
                                let errorMessage = try? response.map(ErrorMessage.self)
                                subscriber.onError(ApiError.badData(error: errorMessage?.message ?? "Unknown error"))
                            }
                            
                        } else if 400 ..< 500 ~= response.statusCode {
                            subscriber.onError(ApiError.badRequest)
                        } else {
                            subscriber.onError(ApiError.serverError)
                        }
                        subscriber.onCompleted()
                }, onError: { (error) in
                    
                    print(error.localizedDescription)
                    subscriber.onError(error)
                })
            
            return Disposables.create()
        }
    }
}
