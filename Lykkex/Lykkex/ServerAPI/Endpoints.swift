//
//  Endpoints.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/17/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import Moya

enum MyRequestService {
    case auth(userName : String, password : String)
    case assetPairs
    case asset
    case lastPrice(assetPairId: String)
}

extension MyRequestService : TargetType{
    var baseURL: URL {
        return URL(string: "https://api-test.lykkex.net/api/")!
    }
    
    var path: String {
        
        switch self {
        case .auth(userName: _, password: _):
            return "auth"
            
        case .assetPairs:
            return "assetPairs"
            
        case .asset:
            return "assets"
            
        case .lastPrice(assetPairId: let id):
            return "markets/\(id)"
        }
    }
    
    var method: Moya.Method {
        
        switch self {
        case .auth(userName: _, password: _):
            return .post
            
        case .assetPairs:
            return .get
            
        case .asset:
            return .get
            
        case .lastPrice(assetPairId: _):
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .auth(userName: let name, password: let pass):
            return .requestParameters(parameters: ["email": name, "password": pass], encoding: JSONEncoding.default)
            
        case .assetPairs:
            return .requestPlain
            
        case .asset:
            return .requestPlain
            
        case .lastPrice(assetPairId: _):
            return .requestPlain
        }
    }
    
    var headers: [String : String]? {
        if let user = KeychainHellper.getLastSavedUser(), let token = user.token{
            return ["Authorization" : "Bearer \(token)"]
        }
        return [:]
    }
    
    
}
