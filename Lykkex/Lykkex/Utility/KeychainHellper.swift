//
//  KeychainHellper.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/18/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

class KeychainHellper{
    
    static func storeUser(user: User, email: String) -> Bool{
        
        guard let encodedData = try? JSONEncoder().encode(user) else{
            return false
        }
        return KeychainWrapper.standard.set(encodedData, forKey: email)
    }
    
    
    static func getLastSavedUser() -> User?{
        
        guard let lastStoredUserEmail = UserDefaults.standard.value(forKey: AppUserDefaults.lastLoginUser.rawValue) as? String else {
            return nil
        }
        
        guard let encodedData = KeychainWrapper.standard.data(forKey: lastStoredUserEmail) else{
            return nil
        }
        
        return try? JSONDecoder().decode(User.self, from: encodedData)
    }
}
