//
//  ValidationUtility.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/17/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation
import RxSwift

class ValidationUtil {
    
    /**
     Validate is format of email correct
     
     - parameter testStr: Email
     
     - returns: Bool value
     */
    static func isValidEmail(_ testStr: String?) -> Bool {
        
        guard testStr != nil else{
            return false
        }
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        
        return emailTest.evaluate(with: testStr)
//        return Observable.just()
    }
}
