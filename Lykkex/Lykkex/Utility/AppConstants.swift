//
//  AppConstants.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/18/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation


enum AppUserDefaults : String{
    
    case lastLoginUser = "LastLoginUser"
}
