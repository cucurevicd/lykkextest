//
//  AssetPair.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/18/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation

struct AssetPairsResponse: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case assetPairs = "AssetPairs"
    }
    
    let assetPairs : [AssetPair]?
    
    init(from decoder: Decoder) throws{
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        assetPairs = try? values.decode([AssetPair].self, forKey: .assetPairs)
    }
}

struct AssetPair: Decodable {
    
    let id: String?
    let accuracy: Int?
    let baseAssetID: String?
    let invertedAccuracy: Int?
    let name, quotingAssetID: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case accuracy = "Accuracy"
        case baseAssetID = "BaseAssetId"
        case invertedAccuracy = "InvertedAccuracy"
        case name = "Name"
        case quotingAssetID = "QuotingAssetId"
    }
    
    init(from decoder: Decoder) throws{
        
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try? values.decode(String.self, forKey: .id)
        accuracy = try? values.decode(Int.self, forKey: .accuracy)
        baseAssetID = try? values.decode(String.self, forKey: .baseAssetID)
        invertedAccuracy = try? values.decode(Int.self, forKey: .invertedAccuracy)
        name = try? values.decode(String.self, forKey: .name)
        quotingAssetID = try? values.decode(String.self, forKey: .quotingAssetID)
    }
}
