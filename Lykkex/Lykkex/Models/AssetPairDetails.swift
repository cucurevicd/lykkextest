//
//  AssetPairdetails.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/18/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation

import Foundation

struct AssetPairDetails: Codable {
    let assetPair: String
    let volume24H, priceChange24H: Int
    let lastPrice, bid, ask: Double
    
    enum CodingKeys: String, CodingKey {
        case assetPair = "AssetPair"
        case volume24H = "Volume24H"
        case priceChange24H = "PriceChange24H"
        case lastPrice = "LastPrice"
        case bid = "Bid"
        case ask = "Ask"
    }
}
