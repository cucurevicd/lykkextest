//
//  User.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/17/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation


struct User : Codable{
    
    enum CodingKeys: String, CodingKey{
        
        case kycStatus = "KycStatus"
        case pinIsEntered = "PinIsEntered"
        case token = "Token"
        case notificationsId = "NotificationsId"
        case personalData = "PersonalData"
        case canCashInViaBankCard = "CanCashInViaBankCard"
        case swiftDepositEnabled = "SwiftDepositEnabled"
        case isUserFromUSA = "IsUserFromUSA"
    }
    
    var kycStatus : String?
    var pinIsEntered : Bool?
    var token : String?
    var notificationsId : String?
    var personalData : PersonalData?
    var canCashInViaBankCard : Bool?
    var swiftDepositEnabled : Bool?
    var isUserFromUSA : Bool?
    
    func encode(to encoder: Encoder) throws
    {
        var containerRoot = encoder.container(keyedBy: RootKeys.self)
        var container = containerRoot.nestedContainer(keyedBy: CodingKeys.self, forKey: .result)
        
//        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(kycStatus, forKey: .kycStatus)
        try container.encode(pinIsEntered, forKey: .pinIsEntered)
        try container.encode(token, forKey: .token)
        try container.encode(notificationsId, forKey: .notificationsId)
        try container.encode(personalData, forKey: .personalData)
        try container.encode(canCashInViaBankCard, forKey: .canCashInViaBankCard)
        try container.encode(swiftDepositEnabled, forKey: .swiftDepositEnabled)
        try container.encode(isUserFromUSA, forKey: .isUserFromUSA)
    }
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: RootKeys.self)
        let values = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .result)
        
        kycStatus = try? values.decode(String.self, forKey: .kycStatus)
        pinIsEntered = try? values.decode(Bool.self, forKey: .pinIsEntered)
        token = try? values.decode(String.self, forKey: .token)
        notificationsId = try? values.decode(String.self, forKey: .notificationsId)
        personalData = try? values.decode(PersonalData.self, forKey: .personalData)
        canCashInViaBankCard = try? values.decode(Bool.self, forKey: .canCashInViaBankCard)
        swiftDepositEnabled = try? values.decode(Bool.self, forKey: .swiftDepositEnabled)
        isUserFromUSA = try? values.decode(Bool.self, forKey: .isUserFromUSA)
    }
}

struct PersonalData : Codable {
    
    enum PersonDataKeys : String, CodingKey{
        
        case fullName = "FullName"
        case firstName = "FirstName"
        case lastName = "LastName"
        case email = "Email"
        case phone = "Phone"
        case country = "Country"
        case city = "City"
        case zip = "Zip"
    }
    
    var fullName : String?
    var firstName : String?
    var lastName : String?
    var email : String?
    var phone : String?
    var country : String?
    var city : String?
    var zip : String?
    
    func encode(to encoder: Encoder) throws
    {
        var container = encoder.container(keyedBy: PersonDataKeys.self)
        try container.encode(fullName, forKey: .fullName)
        try container.encode(firstName, forKey: .firstName)
        try container.encode(lastName, forKey: .lastName)
        try container.encode(email, forKey: .email)
        try container.encode(phone, forKey: .phone)
        try container.encode(country, forKey: .country)
        try container.encode(city, forKey: .city)
        try container.encode(zip, forKey: .zip)
    }
    
    init(from decoder: Decoder) throws {
        
        let values = try decoder.container(keyedBy: PersonDataKeys.self)
        
        fullName = try? values.decode(String.self, forKey: .fullName)
        firstName = try? values.decode(String.self, forKey: .firstName)
        lastName = try? values.decode(String.self, forKey: .lastName)
        email = try? values.decode(String.self, forKey: .email)
        phone = try? values.decode(String.self, forKey: .phone)
        country = try? values.decode(String.self, forKey: .country)
        city = try? values.decode(String.self, forKey: .city)
        zip = try? values.decode(String.self, forKey: .zip)
    }
}
