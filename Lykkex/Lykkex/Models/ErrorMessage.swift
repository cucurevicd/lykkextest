//
//  ErrorMessage.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/17/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation

enum RootKeys: String, CodingKey{
    case result = "Result"
    case error = "Error"
}

struct ErrorMessage : Decodable{
    
    enum CodingKeys: String, CodingKey
    {
        case code = "Code"
        case field = "Field"
        case message = "Message"
        case details = "Details"
    }
    
    var code: Int?
    var field: Int?
    var message: String?
    var details: String?
    
    init(from decoder: Decoder) throws
    {
        let container = try decoder.container(keyedBy: RootKeys.self)
        let values = try container.nestedContainer(keyedBy: CodingKeys.self, forKey: .error)
        code = try? values.decode(Int.self, forKey: .code)
        field = try? values.decode(Int.self, forKey: .field)
        message = try? values.decode(String.self, forKey: .message)
        details = try? values.decode(String.self, forKey: .details)
    }
}
