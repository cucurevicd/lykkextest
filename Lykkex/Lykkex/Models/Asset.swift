//
//  Asset.swift
//  Lykkex
//
//  Created by Dusan Cucurevic on 9/18/18.
//  Copyright © 2018 Home. All rights reserved.
//

import Foundation

struct AssetsResponse : Codable {
    
    enum CodingKeys: String, CodingKey {
        case assets = "Assets"
    }
    
    let assets: [Asset]?
}

struct Asset: Codable {
    let id, blockchainID, name, symbol: String?
    let idIssuer: String?
    let accuracy: Int?
    let hideWithdraw, hideDeposit: Bool?
    let defaultOrder: Int?
    let kycNeeded: Bool?
    let categoryID: String?
    let visaDeposit, swiftDeposit, blockchainDeposit, otherDeposit: Bool?
    let buyScreen, sellScreen, blockchainWithdrawal, swiftWithdrawal: Bool?
    let forwardWithdrawal, crosschainWithdrawal: Bool?
    let bcnDepositAddress: String?
    let bcnDepositAddressExtension: BcnDepositAddressExtension?
    let forwardBaseAsset: String?
    let forwardFrozenDays: Int?
    let forwardMemoURL, assetType, iconURL, displayID: String?
    let blockchain: String?
    let description: Description?
    let isTrusted, privateWalletsEnabled: Bool?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case blockchainID = "BlockchainId"
        case name = "Name"
        case symbol = "Symbol"
        case idIssuer = "IdIssuer"
        case accuracy = "Accuracy"
        case hideWithdraw = "HideWithdraw"
        case hideDeposit = "HideDeposit"
        case defaultOrder = "DefaultOrder"
        case kycNeeded = "KycNeeded"
        case categoryID = "CategoryId"
        case visaDeposit = "VisaDeposit"
        case swiftDeposit = "SwiftDeposit"
        case blockchainDeposit = "BlockchainDeposit"
        case otherDeposit = "OtherDeposit"
        case buyScreen = "BuyScreen"
        case sellScreen = "SellScreen"
        case blockchainWithdrawal = "BlockchainWithdrawal"
        case swiftWithdrawal = "SwiftWithdrawal"
        case forwardWithdrawal = "ForwardWithdrawal"
        case crosschainWithdrawal = "CrosschainWithdrawal"
        case bcnDepositAddress = "BcnDepositAddress"
        case bcnDepositAddressExtension = "BcnDepositAddressExtension"
        case forwardBaseAsset = "ForwardBaseAsset"
        case forwardFrozenDays = "ForwardFrozenDays"
        case forwardMemoURL = "ForwardMemoUrl"
        case assetType = "AssetType"
        case iconURL = "IconUrl"
        case displayID = "DisplayId"
        case blockchain = "Blockchain"
        case description = "Description"
        case isTrusted = "IsTrusted"
        case privateWalletsEnabled = "PrivateWalletsEnabled"
    }
}

struct BcnDepositAddressExtension: Codable {
    let addressExtensionDisplayName, baseAddressDisplayName, depositAddressExtension, typeForDeposit: String?
    let typeForWithdrawal: String?
    
    enum CodingKeys: String, CodingKey {
        case addressExtensionDisplayName = "AddressExtensionDisplayName"
        case baseAddressDisplayName = "BaseAddressDisplayName"
        case depositAddressExtension = "DepositAddressExtension"
        case typeForDeposit = "TypeForDeposit"
        case typeForWithdrawal = "TypeForWithdrawal"
    }
}

struct Description: Codable {
    let id, assetClass: String?
    let popIndex: Int?
    let description, issuerName: String?
    let numberOfCoins: Int?
    let marketCapitalization, assetDescriptionURL, fullName: String?
    
    enum CodingKeys: String, CodingKey {
        case id = "Id"
        case assetClass = "AssetClass"
        case popIndex = "PopIndex"
        case description = "Description"
        case issuerName = "IssuerName"
        case numberOfCoins = "NumberOfCoins"
        case marketCapitalization = "MarketCapitalization"
        case assetDescriptionURL = "AssetDescriptionUrl"
        case fullName = "FullName"
    }
}
